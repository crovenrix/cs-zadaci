import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  public value: Date [] = [new Date ("05/25/2020")]
  public multiSelect: Boolean = true;
  public weekStart: number = 1;
  consturctor () {}
}
