import { Component, ViewChild } from '@angular/core';
import { View, EventSettingsModel, DragEventArgs, ResizeEventArgs, ScheduleComponent, CellClickEventArgs, ActionEventArgs } from '@syncfusion/ej2-angular-schedule';
import { DragAndDropEventArgs } from '@syncfusion/ej2-angular-navigations';
import { L10n } from '@syncfusion/ej2-base';

L10n.load({
  'en-US': {
    'schedule': {
      'title': 'Ime događaja',
      'day': 'Dan',
      'week': 'Tjedan',
      'workWeek': 'Radni tjedan',
      'month': 'Mjesec',
      'today': 'Danas',
      'saveButton': 'Dodaj',
      'cancleButton': 'Zatvori',
      'deleteButton': 'Ukloni',
      'newEvent': 'Dodaj događaj',
      'deleteEvent': 'Obriši događaj',
      'delete': 'Obriši',
      'deleteMultipleEvent': 'Obriši više događaja!',
      'edit': 'Uredi',
      'editSeries': 'Uredi seriju',
      'editEvent': 'Uredi događaj',
      'editContent': 'Želite li urediti ovaj događaj ili cijelu seriju?',
      'deleteRecurrenceContent': 'Želite li obrisati samo ovaj događaj ili seriju seriju?',
      'deleteContent': 'Jeste li sigurni da želite obrisati ovaj događaj',
      'deleteMultipleContent': 'Jeste li sigurni da želite obrisati selektirane događaje?',
    }
  }
});

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Kalendar3';

  @ViewChild('scheduleObj')
  public scheduleInstance: ScheduleComponent;
 /*
  public dateParser(data: string) {
    return new Date(data);
  }
  public statusFields: Object = { text: 'StatusText', value: 'StatusText' };
  public StatusData: Object[] = [
    { StatusText: 'New' }
  ]
  */

  public allowMultipleResource: Boolean = true;
  public resourceDataSource: Object[] = [
    { Name: 'Ivana', Id: 1, Color: 'red', GroupID: 1 },
    { Name: 'Pero', Id: 2, Color: 'green', GroupID: 2 },
    { Name: 'Ivica', Id: 3, Color: 'yellow', GroupID: 2 }
  ]
  public GroupDataSource: Object[] = [
    { Name: 'Odbijen', Id: 1, Color: '#ffaa00', GroupID: 1 },
    { Name: 'Cekanje za odobrenje', Id: 2, Color: '#f8a398', GroupID: 2 },
    { Name: 'Prihvacen', Id: 3, Color: '#7499e1', GroupID: 2 }
  ]
   
  public setView: View = 'Month'; //postavljanje početnog pogleda na kalendar, izmjena izmedu Day, Week, WorkWeek, Month, Agenda
  public setDate: Date = new Date(2020,4,28); //postavljanje datuma po želji
  public dateFormat: string = "dd/MM/yyyy";
  public views: Array<string> = ['Day','Week','WorkWeek','Month'];

  public eventObject: EventSettingsModel = {
    dataSource: 
    [{
      Id: 1,
      Subject: "Testing",
      StartTime: new Date(2020,4,25,10,0), //rucno postavljanje eventa
      EndTime: new Date(2020,4,25,12,0),
      Location: "Rupa",
      PacijentID: 1,
      ZahtjevID: 1
    },
    {
      Id: 2,
      Subject: "Testing2",
      StartTime: new Date(2020,4,28,10,0),
      EndTime: new Date(2020,4,28,12,0),
      Location: "Krov",
      PacijentID: 3,
      ZahtjevID: 2
    }
    ], 
    /*fields: {
      subject: { name: 'Subject', default: "Pozdrav" }
    }*/
  }

  onTreeDragStop(args: DragAndDropEventArgs): void {
    let cellData: CellClickEventArgs = this.scheduleInstance.getCellDetails(args.target);
    let eventData: { [key: string]: Object } = {
      Subject: args.draggedNodeData.text,
      StartTime: cellData.startTime,
      EndTime: cellData.endTime,
      IsAllDay: cellData.isAllDay
    };
    this.scheduleInstance.addEvent(eventData);
  }

  onDragStart(args: DragEventArgs): void {
    args.interval = 5;
    args.navigation.enable = true;
  }
  
  onResizeStart(args: ResizeEventArgs): void {
    args.interval = 5;
  }

  public waitingList: { [key: string]: Object }[] = [
    {
      Id: 2,
      Name: 'Sven'
    },
    {
      Id: 3,
      Name: 'Petar'
    },
    {
      Id: 4,
      Name: 'Ivan'
    }];

    public field: Object = { dataSource: this.waitingList, id: 'Id', text: 'Name'};
  
}
