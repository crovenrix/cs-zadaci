export interface Appointment{
    doctorId: string;
    patientId: string;
    dateTime: string;
    status: string;
}