export interface Patient{
    Id: string;
    Name: string;
    Surname: string;
    Email: string;
}